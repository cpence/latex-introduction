# A Philosopher's Introduction to LaTeX

*[Charles H. Pence](http://www.charlespence.net)*

So you want to typeset a document with lots of mathematical or logical formulas, and you don't want it to look like `x + y^2 = 4z`. No, you actually want it to look like your textbook. Well, you're in luck. In all likelihood, the program the authors of your textbook used to write it is a handy little tool called LaTeX ("LAY-tech"). There's all sorts of history here involving TeX's author (Knuth), the programming of TeX, the book that is the TeX source code (yes, the entire source code to TeX is actually a book that you can buy, and it's even readable), but that's for another time and place. Or some kind of computer science lecture or something. You just want to draw pretty integrals, right? Right. So let's get to it.

Before we start, a brief note. This isn't necessarily the easiest thing to do in the world, nor is TeX the easiest thing to learn. However, if you don't have too solid of computer skills, this is a great time to start learning. So, even if you have to struggle through this, it'll be good for you. No, really. Like eating your vegetables.

To get LaTeX, go to the [TeX Live](http://www.tug.org/texlive/) website if you have Windows, the [MacTeX](http://www.tug.org/mactex/) website for a Macintosh (running OS X), and either the TeX Live website or your package manager if you're running Linux. Get the full TeX Live package, and install it (warning: this is a large download and a long install).

Now you need a good editor in which to write TeX documents. [TeXworks](http://tug.org/texworks/) should come with TeX Live and be available on every single platform, and it's really easy to use. If it doesn't strike your fancy, on Windows I like [WinEdt](http://www.winedt.com/), and people have recommended [TeXnicCenter](http://www.texniccenter.org).  Also, all of the standard cross-platform text editors have really nice TeX support, including Emacs, Vim, Sublime Text, and Github Atom.

So, with that, let's write our very first TeX document. Fire up your editor and fill a new document with the following:

~~~ latex
\documentclass[letterpaper,12pt]{article}

\usepackage[left=1in,right=1in,top=1.25in,bottom=1.25in]{geometry}
\usepackage[T1]{fontenc}
\usepackage{amsmath,amsthm}
\usepackage{amsfonts}
\usepackage{amssymb}

\newtheorem{thm}{Theorem}
\newtheorem{prp}{Proposition}
\newtheorem{lem}{Lemma}

\title{How to Take Over the World in 30 Days}
\author{Dr. Claw}

\begin{document}
\maketitle

\section{Getting Started}

Firstly, you need a world-domination plan. These may be
purchased at your local bookstore, or the very creative
can come up with one on their own.

\end{document}
~~~

Save this file as paper.tex (I'd recommend having a different folder somewhere on your computer for *each* TeX document you make, because TeX will produce a lot of output files). Now, compile the document. [TeXworks: click the green play arrow button; WinEdt: click the brown-dog-thing with a red curly-cue behind it; Mac: click the 'Typeset' button; Linux: go to the command-line and type `pdflatex paper.tex`.] This will magically create a PDF file out of the above document. [TeXworks and WinEdt will open it for you automatically, Linux users run `xpdf paper.tex`.] Astute readers will immediately recognize the font here as that in which every single PDF set of lecture notes for every math class is written. Several things about the template we've already got. The first line tells LaTeX that you're writing an article, which is the right kind of document for most academic papers, and sets the font size and paper size. The next block of five lines loads up *packages,* which, in TeX parlance, are add-ons that do different things. The first sets page margins, the second is a technical upgrade for nicer fonts, and the last three load useful math packages. The `\newtheorem` lines create things we'll use later to write Theorems, Propositions, and Lemmas (shock). The title and author lines are processed by the `\maketitle` command below to produce the title block on the first page, so set them accordingly. The `\begin{document}` and `\end{document}` are the first and last lines of the actual paper. Right after `\begin{document}` should always be `\maketitle`, so TeX can write your title block/title page in your document. Now, we get to actually writing LaTeX.

First off, your document is divided into sections by the `\section` command. We've defined one section right off. You can define as many as you want -- try copying and pasting the section command/paragraph and changing the section header. You'll note that TeX numbers them automatically and formats them specially, all without you actually having to tell it how to do that formatting. Starting to see the beauty of the system? Good. Now, straight text in LaTeX is pretty easy. First of all, you can't add white space between lines or paragraphs just by pressing return. Try typing the following lines to get a feel for how the paragraphs work in TeX:

~~~ latex
This
is
all
on
one
paragraph

This is all on a new paragraph.

This too is a new paragraph. Note the blank line between
the lines of text. That makes it start a new paragraph.
But one
return
doesn't.

See though, even though I try to put



Lots of space between paragraphs



It doesn't do anything.
~~~

This is one of the guiding principles of TeX -- how you format things in your TeX file has little or nothing to do with the exact formatting of the final document. Get used to it, it's actually kinda nice.  Okay, I hear you say, now I can write great essays in this thing, but where's the math? Right, right, here we go then. First off, you should know that there are basically three ways to type math in a TeX file. Here's a little demonstration:

~~~ latex
This math will be typeset $x+5 = y-3$ right in the middle
of the paragraph, or what's called ``inline''. Also, notice that
those quotation marks are funny. That makes TeX make nice
typographically-correct quotation marks, so use those things
instead of shift-apostrophe. This formula coming up will be
numbered and centered:

\begin{equation}
x^2 + 4(x - 5)^4 = 3^{x^2}
\end{equation}

If you don't want it numbered, add a star to the equation
environment, like this:

\begin{equation*}
x^2 + 4(x - 5)^4 = 3^{x^2}
\end{equation*}
~~~

Note that the math code between `$$` is inline, and between `\begin/end{equation}` is typeset in the middle of the page, but there's absolutely no difference between the math code between those delimiters -- i.e. if you see me write somewhere else in the document that you do something between dollar signs or between equation tags, you can still use it between equation tags or dollar signs, respectively -- these just change how the math is placed on the page. There's also a more complicated environment you can use to write multiple equations in a row and line them up vertically on their equals sign, but it's beyond the scope of this guide. Google "latex align" and you'll find it.

So there's a lot of math syntax to digest there. First, you can see that the simple syntax for raising things to a power is `x^2` (that's caret, or shift–6 on your keyboard). Also, if you want to raise to a power that's not just a number, you have to put the argument in curly braces, like `3^{x^2}` or `a^{(p - 1)/2}`. You should also be shocked at how automatic all of the formatting of this math is -- you don't have to do anything but type the formula. Now, I think more math formatting is best taught by example, so here we go. I'd recommend you copy these examples into your TeX file, compile it, and read the PDF along with the TeX source:

~~~ latex
Here's one with some greek letters and subscripts, so
you get an idea of how those work. The subscripts use an
underscore instead of a caret, but work basically the same way.
Note how the subscript-superscript combo works:

\begin{equation}
2^\alpha = \phi_0 = b_0^2
\end{equation}

Now for number theory. First, to obtain the symbols for integers,
rationals, etc., we type $\mathbb{Q}$, $\mathbb{C}$,
$\mathbb{R}$, or $\mathbb{Z}/p\mathbb{Z}$. You get the
idea. LaTeX has a builtin mod function, but it produces horrible
output, so the thing to do is as follows. Note importantly the
space after ``mod'' inside the text command, and that equiv
creates the fancy triple-bar equals:

\begin{equation}
a \equiv 1 (\text{mod } p)
\end{equation}

Here's an integral. Note that you set definite integral limits with
sub and superscripts just like a variable. Also, using the sin with
the backslash makes it typeset properly. The backslash-comma
adds a little bit of space between the sin x and the dx to make it
look right. Also note the square root.

\begin{equation}
0 = \int_0^{2\pi} \sqrt{ \sin x }\, dx
\end{equation}

Here's a summation, which works basically the same way. Also
notice here we introduce how to do real fractions, which also
works for the Legendre symbol:

\begin{equation}
\sum_{k=1}^n k^2 = \frac{1}{2} n (n+1)
\end{equation}

And, if you're doing any advanced calculus, here's how to a
second derivative of u with respect to x, and then a second
partial derivative of u with repsect to x:

\begin{equation}
\frac{d^2 u}{dx^2} = \frac{\partial^2 u}{\partial x^2}
\end{equation}

The same subscripting technique as before also works for
limits:

\begin{equation}
\lim_{x \to \infty} \frac{1}{x} = 0
\end{equation}
~~~

If you're interested in formal logic rather than just mathematics, check out the following list of logical symbols. You also can see here that `\\` will insert a hard return into a paragraph.

~~~ latex
$\neg$ : unary not (the `hook' variety) \\
$\mathord{\sim}$ : unary not (the `tilde' variety) \\
$\square$ : modal `box' \\
$\lozenge$ : modal `diamond' \\
$\lor$ : wedge \\
$\land$ : caret \\
$\supset$ : horseshoe \\
$\equiv$ : tribar

$\otimes$ : circled X (closed tree branch)
~~~

*UPDATE 2/4/08:* I've added a formatted symbol (using the custom math symbol commands) thanks to Kevin Klement that provides a proper "tilde" unary-not symbol, in addition to the "hook" unary-not symbol that was already listed. Thanks!

If you need some kind of random symbol that's not listed above, it's almost certain that it's available in LaTeX. Download [this massive PDF file](ftp://tug.ctan.org/pub/tex-archive/info/symbols/comprehensive/symbols-letter.pdf), and you'll have a list of basically everything you could ever want.

This is about all the examples that I can think of right now. Google is your friend with LaTeX -- much has been written about how this system works, and it's pretty easy to find more information. I hope this managed to make some sense, and you'll find it useful. LaTeX is the best computer program since the C compiler, and I hope you find using it as easy and as holistically life-fulfilling as I do. Or something. Go write brilliant math!
